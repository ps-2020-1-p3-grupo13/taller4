#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>


int main(int argc, char * argv[]){

        if(argc==3){
                int fd =  open(argv[1],O_RDONLY,0666);
                int fd2 = open(argv[2],O_WRONLY | O_CREAT | O_TRUNC,0666);
                if(fd<0){
                        perror("Error!!!");
	                exit(-1);
                } else{
                        long int cantidad_bytes = lseek(fd,0,SEEK_END);
                        lseek(fd,0,SEEK_SET);
			if(cantidad_bytes>0){
                        	int n;
                                char buf[100000];
                                while((n = read(fd,buf,100000))>0){
                                        write(fd2,buf,n);
                                }
                                close(fd);
                                close(fd2);
                                printf("%ld bytes copiados \n",cantidad_bytes);
                        } else{
                        	return 0;
                        }
                }
        } else{
        	return 0;
        }
	return 0;
}
