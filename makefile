bin/pscopy: obj/main.o
	gcc -Wall obj/main.o -o bin/pscopy

obj/main.o: src/main.c
	gcc -Wall -c src/main.c -o obj/main.o

.PHONY: clean
clean:
	rm -r bin obj
	mkdir bin obj
